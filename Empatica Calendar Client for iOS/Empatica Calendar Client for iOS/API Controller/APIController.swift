//
//  APIController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 19/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import SwiftyJSON

class APIController: NSObject {
    
    let totalCaregivers = 100
    let maxResultsPerRequest = 10
    
    let usersAddress: String = "https://randomuser.me/api/?seed=empatica&page="
    let resultsSufix: String = "&results="
    
    var tempCaregivers: Array<TemporaryCaregiver>?
    
    // MARK: - Properties
    private static var sharedAPIController: APIController = {
        let apiController = APIController()
        
        return apiController
    }()
    
    // MARK: - Accessors
    class func shared() -> APIController {
        return sharedAPIController
    }
    
    // MARK: - Inits
    override init(){
        super.init()
        
    }
    
    func reloadCache(){
        DispatchQueue.main.async {
            self.tempCaregivers = []
            self.getUsersFromWeb(currentPage: 1)
        }
    }
    
    func getUsersFromWeb(currentPage: Int) {
        let maxIteration: Int = Int(totalCaregivers/maxResultsPerRequest)
        if(currentPage > maxIteration){
            self.cacheTempCaregivers()
        }else{
            let dynamicURLString: String = usersAddress + currentPage.description + resultsSufix + maxResultsPerRequest.description
        
            guard let url = URL(string: dynamicURLString) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil { print(error!.localizedDescription) }
                guard let data = data else { return }
                do {
                    let json = try? JSON(data: data)
                    for i in 0...(self.maxResultsPerRequest - 1) {
                        if let userInfo = json?["results"][i] {
                            //fill caregivers info with ones found on JSON request, each user found at a time
                            let caregiverFirstName = userInfo["name"]["first"].string ?? "John"
                            let caregiverLastName = userInfo["name"]["last"].string ?? "Doe"
                            let caregiverName: String = (caregiverFirstName + " " + caregiverLastName).capitalized
                            
                            let caregiverNumber: Int? = (i + 1) + ((currentPage - 1) * self.maxResultsPerRequest)
                            let caregiverImagePath: String? = userInfo["picture"]["thumbnail"].string
                            self.tempCaregivers?.append(TemporaryCaregiver(withName: caregiverName, andWithNumber: caregiverNumber, andWithImagePath: caregiverImagePath))
                        }
                    }
                    self.getUsersFromWeb(currentPage: currentPage + 1)
                }
            }.resume()
        }
    }
    
    // - Mark: Entities' Cache Related Method
    
    func cacheTempCaregivers(){
        //transform all image URLs into actual files on the user's devices
        DispatchQueue.global().async {
            for tempCaregiverUnit in self.tempCaregivers! {
                //download image based on given URL from API GET REST response
                let data = try? Data(contentsOf: URL(string: tempCaregiverUnit.image!)!)
                DispatchQueue.main.async {
                    let thumbnailImageTemp: UIImage = UIImage(data: data!)!
                    
                    //save image file into iOS' documents folder
                    let fileManager = FileManager.default
                    do {
                        let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                        let imageName: String = "caregiver_profile_\(tempCaregiverUnit.number!)"
                        let fileURL = documentDirectory.appendingPathComponent(imageName + ".jpg") 
                        if let imageData = thumbnailImageTemp.jpegData(compressionQuality: 1.0) {
                            try imageData.write(to: fileURL)
                            tempCaregiverUnit.image = imageName + ".jpg"
                            
                            if(tempCaregiverUnit.number! >= self.totalCaregivers){
                                self.persistNewInformation()
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
            }
        }
        
        
    }
    
    func persistNewInformation(){
        let allCaregivers: Array<Caregiver> = CalendarDataController.shared().getAllCaregivers() ?? []
        
        //remove temporary caregivers that did not change any information
        for caregiverUnit in allCaregivers {
            let index: Int? = self.tempCaregivers?.firstIndex(where: { $0.number! == Int(caregiverUnit.number) && ($0.image == caregiverUnit.image) && ($0.name == caregiverUnit.name)})
            if(index != nil){
                self.tempCaregivers?.remove(at: index!)
            }
        }
        
        //add new/updated Caregivers infos into local memory
        CalendarDataController.shared().fillCachedCaregivers(fromTemporaryArray: self.tempCaregivers!)
        
        //and also add new/updated Caregivers infos into Calendar CoreDB
        _ = CoreDataController.shared().saveCaregiversIntoCoreDataDB()
        
        print("Finished Caching all new Caregiver's info")
    }
}
