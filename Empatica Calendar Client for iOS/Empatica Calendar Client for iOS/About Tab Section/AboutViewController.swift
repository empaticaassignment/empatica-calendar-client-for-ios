//
//  SecondViewController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 17/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func clearAllAssignmentsBtnTouched(_ sender: UIButton) {
        CalendarDataController.shared().clearAllAssignments()
    }
    
}

