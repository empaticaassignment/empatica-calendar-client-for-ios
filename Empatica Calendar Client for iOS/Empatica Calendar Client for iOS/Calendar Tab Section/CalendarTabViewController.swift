//
//  CalendarTabViewController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 19/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import CalendarKit

class CalendarTabViewController: UIViewController, DatePickerControllerDelegate {
    
    var calendarUIViewController: CalendarController = CalendarController()
    
    @IBOutlet var calendarUIView: UIView!
    
    @IBOutlet var topLeftUIButton: UIButton!
    @IBOutlet var topRightUIButton: UIButton!
    
    @IBOutlet var loadingAutoAssignIndicator: UIActivityIndicatorView!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if(self.calendarUIView.subviews.contains(calendarUIViewController.dayView) == false){
            self.calendarUIView.addSubview(calendarUIViewController.dayView)
            self.calendarUIViewController.rootController = self
        }
        calendarUIViewController.dayView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: calendarUIView.frame.size)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(CaregiverAutoAssignController.shared().isWorking == true){
            //should still indicate that AutoAssign is still working on its tasks
            if(self.loadingAutoAssignIndicator.isAnimating == false){
                self.loadingAutoAssignIndicator.startAnimating()
            }
            self.loadingAutoAssignIndicator.isHidden = false
            self.topRightUIButton.isEnabled = false
        }else{
            if(self.loadingAutoAssignIndicator.isAnimating == true){
                self.loadingAutoAssignIndicator.stopAnimating()
            }
            self.loadingAutoAssignIndicator.isHidden = true
            self.topRightUIButton.isEnabled = true
        }
        
        self.calendarUIViewController.calendarSubviewWillAppear()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Set Actions for the two Top Buttons
    //Left Top Button: Change Date
    @IBAction func leftTopButtonTouched(_ sender: UIButton) {
        //Change Date button clicked, present Date Picker Navigation View Controller
        self.presentDatePicker()
    }
    
    //Right Top Button: Auto Assign (AI assignment pick mode)
    @IBAction func rightTopButtonTouched(_ sender: UIButton) {
        //Auto Assign Button touched, send to handler
        loadingAutoAssignIndicator.isHidden = false
        topRightUIButton.isEnabled = false
        self.loadingAutoAssignIndicator.startAnimating()
        DispatchQueue.global().async() {
            self.calendarUIViewController.AutoAssignButtonTouched()
            
            DispatchQueue.main.async {
                [weak self] in
                
                guard let strongSelf = self else {return}
                strongSelf.loadingAutoAssignIndicator.stopAnimating()
                strongSelf.loadingAutoAssignIndicator.isHidden = true
                strongSelf.topRightUIButton.isEnabled = true
                
                strongSelf.calendarUIViewController.reloadEventsForDate()
            }
        }
    }
    
    func presentDatePicker() {
        let picker = DatePickerController()
        picker.date = calendarUIViewController.dayView.state!.selectedDate
        picker.delegate = self
        
        let navC = UINavigationController(rootViewController: picker)
        present(navC, animated: true, completion: nil)
    }
    
    func datePicker(controller: DatePickerController, didSelect date: Date?) {
        if let date = date {
            calendarUIViewController.dayView.state?.move(to: date)
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "newAssignment") {
            // config when it is a new event being freshly created
            let secondViewController = segue.destination as! AssignmentViewController
            if(sender != nil){
                let dateToSend = sender as? Date
                secondViewController.datePicked = dateToSend
            }
        } else if(segue.identifier == "eventDetails") {
            // config when it is a already existing event being open again in a details' view controller
            let secondViewController = segue.destination as! AssignmentViewController
            if(sender != nil){
                let eventToSend = sender as? EventView
                secondViewController.eventDetails = eventToSend
            }
        }
    }
}
