//
//  CalendarDataController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 19/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import CoreData

class CalendarDataController: NSObject {
    var temporaryAssignment: TemporaryAssignment?
    
    var assignmentsDisplaying: Array<Assignment>? = []
    var roomsDisplaying: Array<Room>? = []
    var caregiversDisplaying: Array<Caregiver>? = []
    
    let maxRoomsPerFloor: Int = 10
    let maxFloors: Int = 1
    
    //Variable to control how much data is retrieved from CoreData DB, and stored locally, related to
    let maxDaysDiff: Int = 2 //(2 days before, 2 days after, and the day fetched) = 5 days total
    
    // MARK: - Singleton Design Pattern Related Methods
    
    // MARK: - Properties
    private static var sharedCalendarData: CalendarDataController = {
        let calendarData = CalendarDataController()
        
        return calendarData
    }()
    
    // MARK: - Accessors 
    class func shared() -> CalendarDataController {
        return sharedCalendarData
    }
    
    // MARK: - Inits
    override init(){
        super.init()
        
    }
    
    // MARK: - Local Memory Main Controllers
    
    func initialFetchBatch(){
        //fech from DB into *displaying memory vars
        self.fillRoomsInfoFromDB()
        self.fillCaregiversInfoFromDB()
        
        fetchAssignments(nearDate: Date(), shouldRefreshContent: true)
    }
    
    // MARK: - Filling Local Memory from CoreDB and, when failed, from local Generic vars' Related Methods
    
    func fillRoomsInfoFromDB(){
        let roomsFetched: Array<Room>? = CoreDataController.shared().fetchRoomsFromDB()
        
        if(roomsFetched != nil){
            //replace *displaying info for Rooms
            self.roomsDisplaying = roomsFetched
        }else{
            //fill rooms with generic temporary ones
            fillGenericRoomsTemporarily()
        }
    }
    
    func fillGenericRoomsTemporarily(){
        roomsDisplaying = []
        
        //fill gneeric rooms, from 1 to max rooms at the hospital
        for i in 1...(maxFloors * maxRoomsPerFloor) {
            let newRoom: Room = Room(context: CoreDataController.shared().getContextDB()!)
            newRoom.roomNo = Int16(i)
            roomsDisplaying?.append(newRoom)
        }
    }
    
    func fillCaregiversInfoFromDB(){
        let caregiversFetched: Array<Caregiver>? = CoreDataController.shared().fetchCaregiversFromDB()
        
        if(caregiversFetched != nil){
            //replace *displaying info for Caregivers
            self.caregiversDisplaying = caregiversFetched
        }else{
            //fill caregivers with temporary ones
            fillGenericCaregiversTemporarily()
        }
    }
    
    func fillGenericCaregiversTemporarily(){
        let totalCaregivers: Int = 100
        
        caregiversDisplaying = []
        
        //fill gneeric rooms, from 1 to max rooms at the hospital
        for i in 1...totalCaregivers {
            let newCaregiver: Caregiver = Caregiver(context: CoreDataController.shared().getContextDB()!)
            newCaregiver.name = "Generic \(i)"
            newCaregiver.number = Int16(i)
            caregiversDisplaying?.append(newCaregiver)
        }
    }
    
    // Mark - Fill into Local Mem from Temporary Vars Array Related Methods
    
    func fillCachedCaregivers(fromTemporaryArray newTemporaryCaregivers:Array<TemporaryCaregiver>){
        //update/add caregivers' local mem with ones containing new/updated info
        for tempCaregiver in newTemporaryCaregivers {
            let index: Int? = self.caregiversDisplaying?.firstIndex(where: { Int($0.number) == tempCaregiver.number! })
            if(index != nil){
                //already has caregiver with number, so just update its Name and Image
                self.caregiversDisplaying?[index!].name = tempCaregiver.name
                self.caregiversDisplaying?[index!].image = tempCaregiver.image
            }else{
                //does not have such caregiver, add new one into local mem
                let newCaregiver: Caregiver = Caregiver(context: CoreDataController.shared().getContextDB()!)
                newCaregiver.name = tempCaregiver.name
                newCaregiver.image = tempCaregiver.image
                newCaregiver.number = Int16(tempCaregiver.number!)
                self.caregiversDisplaying!.append(newCaregiver)
            }
        }
    }
    
    func fillCachedRooms(fromTemporaryArray newTemporaryRooms: Array<TemporaryRoom>){
        //clear rooms' local mem
        roomsDisplaying = []
        
        //fill rooms' local mem with new info
        for tempRoom in newTemporaryRooms {
            let newRoom: Room = Room(context: CoreDataController.shared().getContextDB()!)
            if(tempRoom.roomNo != nil){
                newRoom.roomNo = Int16(tempRoom.roomNo!)
            }
            roomsDisplaying!.append(newRoom)
        }
    }
    
    // MARK: - Fetch from Local Memory (and, when necessary, requests for CoreDB) Related Methods
    
    func getAllCaregivers() -> Array<Caregiver>?{
        return self.caregiversDisplaying
    }
    
    func getCaregiver(withNumber numberToFetch: Int) -> Caregiver?{
        for caregiverUnit in self.caregiversDisplaying ?? [] {
            if(Int(caregiverUnit.number) == numberToFetch){
                return caregiverUnit
            }
        }
        return nil
    }
    
    func fetchAssignments(nearDate date:Date, shouldRefreshContent shouldRefresh:Bool?){
        //fetch assignments near the date specified by a maximum difference from date
        //create first a list of dates to fetch (5 days in single full kickoff hour each)
        var datesToFetch: Array<Date> = []
        let kickoffDate: Date = date.startOfDay
        datesToFetch.append(kickoffDate)
        for i in 1...maxDaysDiff {
            datesToFetch.append(Calendar.current.date(byAdding: .day, value: i, to: kickoffDate)!)
            datesToFetch.append(Calendar.current.date(byAdding: .day, value: -i, to: kickoffDate)!)
        }
        
        //clear assignmentsDisplaying array if needs to refresh
        if(shouldRefresh == true){
            assignmentsDisplaying = []
        }
        
        //check if there is already fetched dates and there is no need to refresh, also check if there is any in re-use, and only fech necessary ones
        if(shouldRefresh != nil && assignmentsDisplaying != nil){
            if(shouldRefresh == false){
                //remove dates that aren't into context of refreshing
                for assignmentDisplaying in assignmentsDisplaying! {
                    let diffInDays: Int = Calendar.current.dateComponents([Calendar.Component.day], from: assignmentDisplaying.time! as Date, to: kickoffDate).day ?? maxDaysDiff
                    if(abs(diffInDays) > maxDaysDiff){
                        //remove it, in case the dates differ more than maxDaysDiff (2) days from kickoff
                        if let indexToRemove = assignmentsDisplaying!.index(of: assignmentDisplaying) {
                            assignmentsDisplaying!.remove(at: indexToRemove)
                        }
                    }else{
                        //check if assignmentDisplaying's date is inside datesToFetch array
                        for dateToCompare in datesToFetch {
                            let startDateOfAssignmentDisplaying: Date = Calendar.current.startOfDay(for: assignmentDisplaying.time! as Date)
                            if(dateToCompare == startDateOfAssignmentDisplaying){
                                //if it is, remove it (there is no need to refresh right now)
                                if let indexToRemove2 = datesToFetch.index(of: dateToCompare) {
                                    datesToFetch.remove(at: indexToRemove2)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //fetch all dates different then dates displaying and passed filters of removal from DB
        let allNewAssignments: Array<Assignment> = CoreDataController.shared().fetchAssignmentsFromDB(fromDatesToFetch: datesToFetch)
        
        //fill info fetched into local memory's assignmentsDisplaying variable
        if(self.assignmentsDisplaying != nil){
            self.assignmentsDisplaying! += allNewAssignments
        }else{
            self.assignmentsDisplaying = allNewAssignments
        }
    }
    
    func createNewAssignment(atDateTime datetime:Date, withCaregiver caregiverNo:Int, andWithRoom roomNo:Int) -> Bool {
        //try to find correct caregiver entity
        var chosenCaregiver: Caregiver? = nil
        for caregiverUnit in caregiversDisplaying! {
            if(caregiverUnit.number == Int16(caregiverNo)){
                //found caregiver
                chosenCaregiver = caregiverUnit
                break
            }
        }
        if(chosenCaregiver == nil){
            //did not find any caregiver, so create a generic one and add to this assignment (should never enter here)
            let caregiverEntity: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "Caregiver", in: CoreDataController.shared().getContextDB()!)
            chosenCaregiver = NSManagedObject(entity: caregiverEntity!, insertInto: CoreDataController.shared().getContextDB()!) as? Caregiver
            chosenCaregiver?.setValue("Generic Caregiver #1", forKeyPath: "name")
            chosenCaregiver?.setValue(caregiverNo, forKeyPath: "number")
        }
        
        //try to find correct room entity
        var chosenRoom: Room? = nil
        for roomUnit in roomsDisplaying! {
            if(roomUnit.roomNo == Int16(roomNo)){
                //found room
                chosenRoom = roomUnit
                break
            }
        }
        if(chosenRoom == nil){
            //did not find any room, so put a generic one (should never enter here...)
            let roomEntity: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "Room", in: CoreDataController.shared().getContextDB()!)
            chosenRoom = NSManagedObject(entity: roomEntity!, insertInto: CoreDataController.shared().getContextDB()!) as? Room
            chosenRoom?.setValue(roomNo, forKeyPath: "roomNo")
        }
        
        //now create new assignment correctly
        let assignmentEntity: NSEntityDescription? = NSEntityDescription.entity(forEntityName: "Assignment", in: CoreDataController.shared().getContextDB()!)
        let newAssignment: Assignment? = NSManagedObject(entity: assignmentEntity!, insertInto: CoreDataController.shared().getContextDB()!) as? Assignment
        newAssignment?.setValue(datetime, forKeyPath: "time")
        
        if(chosenCaregiver != nil && newAssignment != nil){
            chosenCaregiver!.addToAssignments(newAssignment!)
        }
        if(chosenRoom != nil && newAssignment != nil){
            chosenRoom!.addToAssignments(newAssignment!)
        }
        
        //Save into CoreDB
        _ = CoreDataController.shared().saveAssignmentIntoCoreDataDB()
        
        return true
    }

    func replaceAssignment(fromOldAssignment oldAssignment: Assignment?, toNewOneWithDate newDatetime: Date, andNewCaregiverNumber newCaregiverNo: Int, andNewRoomNymber newRoomNo: Int, andNewColor newColor: Int){
        //check if there was any changes in thre main variables comparing with cached one
        if(oldAssignment != nil && oldAssignment?.time != nil){
            //did time and/or caregiver and/or room variable(s) changed?
            if((oldAssignment!.time!.isEqual(to: newDatetime) == false) ||
                (oldAssignment!.caregiverAssigned?.number != Int16(newCaregiverNo)) ||
                (oldAssignment!.roomAssigned?.roomNo != Int16(newRoomNo))){
                //it did change, so remove from cache and CoreDB
                self.removeAssignment(assignment: oldAssignment!)
                
                //create assingment with correct info now
                _ = self.createNewAssignment(atDateTime: newDatetime, withCaregiver: newCaregiverNo, andWithRoom: newRoomNo)
            }
        }
    }
    
    //- Mark: Temporary Variables' filling
    
    func addTemporaryAssignment(forTime datetime:Date){
        temporaryAssignment = TemporaryAssignment()
        temporaryAssignment?.aiGen = false
        temporaryAssignment?.time = datetime as NSDate?
        temporaryAssignment?.roomAssigned = nil
        temporaryAssignment?.caregiverAssigned = nil
        temporaryAssignment?.color = Int.random(in: 0...5)
    }
    
    func fillTemporaryAssignmentAndSave(withRoomNumber roomNo:Int, andCaregiverNumber caregiverNo:Int, atDate newDatetime:Date, andwithAssignmentByAI assignedByAI: Bool, andWithColor newColor: Int?){
        //get correct Room Obj DB in local memory
        var roomObjDB: Room?
        for roomUnit in roomsDisplaying! {
            if(roomUnit.roomNo == roomNo){
                roomObjDB = roomUnit
                break
            }
        }
        
        //get correct Caregiver Obj DB in local memory
        var caregiverObjDB: Caregiver?
        for caregiverUnit in caregiversDisplaying! {
            if(caregiverUnit.number == caregiverNo){
                caregiverObjDB = caregiverUnit
                break
            }
        }
        
        //fill all objects correctly into new object and cancel "temporary Assignment"
        let newAssignment: Assignment? = Assignment(context: CoreDataController.shared().getContextDB()!)
        newAssignment?.time = newDatetime as NSDate
        if(caregiverObjDB != nil && newAssignment != nil){
            caregiverObjDB!.addToAssignments(newAssignment!)
        }
        if(roomObjDB != nil && newAssignment != nil){
            roomObjDB!.addToAssignments(newAssignment!)
        }
        newAssignment?.aiGen = assignedByAI
        
        if(temporaryAssignment != nil){
            newAssignment?.color = Int32(temporaryAssignment!.color!)
        }else if(newColor != nil){
            newAssignment?.color = Int32(newColor!)
        }else{
            newAssignment?.color = Int32(Int.random(in: 0...5))
        }
        
        //save new real assignment properly filled into CoreDataDB
        _ = CoreDataController.shared().saveAssignmentIntoCoreDataDB()
        temporaryAssignment = nil
        
    }
    
    func getAssignments(forDate datetime: Date) -> Array<Assignment>? {
        var assignmentsMatching : Array<Assignment> = []
        if(assignmentsDisplaying != nil){
            for assignmentUnit in assignmentsDisplaying! {
                if(assignmentUnit.time != nil){
                    let dayBeginning: Date = assignmentUnit.time! as Date
                    if(dayBeginning.startOfDay == datetime.startOfDay){
                        assignmentsMatching.append(assignmentUnit)
                    }
                }
            }
        }
        return assignmentsMatching
    }
    
    func getAssignmentsAtSlot(ofHourAtDate datetime: Date) -> Array<Assignment>?{
        var assignmentsMatching : Array<Assignment> = []
        if(assignmentsDisplaying != nil){
            for assignmentUnit in assignmentsDisplaying! {
                if(assignmentUnit.time != nil){
                    let assignmentUnitDate: Date = assignmentUnit.time! as Date
                    if(assignmentUnitDate == datetime){
                        assignmentsMatching.append(assignmentUnit)
                    }
                }
            }
        }
        return assignmentsMatching
    }
    
    func getTemporaryAssignment(ifMatchesDateCriteria datetime: Date) -> TemporaryAssignment?{
        if(temporaryAssignment != nil && temporaryAssignment?.time != nil){
            let tempDate = temporaryAssignment!.time! as Date
            if(tempDate.startOfDay == datetime.startOfDay){
                return temporaryAssignment!
            }
        }
        return nil
    }
    
    func getSpecificAssignment(withSpecificDate datetime:Date, andWithSpecificRoomNo roomNo:Int, andWithSpecificCaregiverNumber caregiverNo: Int) -> Assignment? {
        if(assignmentsDisplaying != nil){
            for assignment in assignmentsDisplaying!{
                if(assignment.time != nil){
                    if((assignment.time!.isEqual(to: datetime)) && (assignment.caregiverAssigned!.number == Int16(caregiverNo)) && (assignment.roomAssigned!.roomNo == Int16(roomNo))){
                        //found specific assignment by matching Date, Room Number and Caregiver assigned
                        return assignment
                    }
                }
            }
        }
        return nil
    }
    
    func removeAssignment(assignment assignmentToRemove: Assignment){
        //first, remove from local memory (in case there is one)
        var indexToRemove: Int = 0
        if(assignmentsDisplaying != nil){
            for assignment in assignmentsDisplaying! {
                if(assignment.time!.isEqual(to: assignmentToRemove.time! as Date) && (assignment.caregiverAssigned!.number == assignmentToRemove.caregiverAssigned!.number) && (assignment.roomAssigned!.roomNo == assignmentToRemove.roomAssigned!.roomNo)){
                    //found it (by matching date, roomNo and Assignment)
                    break
                }
                indexToRemove += 1
            }
            
            //remove it from assignmentsDisplaying's list
            assignmentsDisplaying!.remove(at: indexToRemove)
        }
        
        //and then, secondly, remove from DB
        _ = CoreDataController.shared().removeAssignmentFromDB(assignment: assignmentToRemove)
    }
    
    func clearAllAssignments(){
        //remove all Assignments in Core Data DB
        _ = CoreDataController.shared().clearAllAssignmentsFromDB()
        
        //clear local memory info
        assignmentsDisplaying = []
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }

}

// - Mark: Temporary Information retainer classes of future Core and Local Memories' Entities

class TemporaryAssignment {
    var aiGen: Bool?
    var time: NSDate?
    var roomAssigned: TemporaryRoom?
    var caregiverAssigned: TemporaryCaregiver?
    var color: Int?
}

class TemporaryCaregiver {
    var name: String?
    var number: Int?
    var image: String?
    
    init(withName newName: String?, andWithNumber newNumber: Int?, andWithImagePath newImagePath: String?) {
        self.name = newName
        self.number = newNumber
        self.image = newImagePath
    }
}

class TemporaryRoom {
    var roomNo: Int?
}
