//
//  CoreDataController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 21/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import CoreData

class CoreDataController: NSObject {
    
    var managedContext: NSManagedObjectContext?
    
    //- Mark: Singleton Design Pattern related methods
    
    // MARK: - Properties
    private static var sharedCoreDataController: CoreDataController = {
        let coreData = CoreDataController()
        
        return coreData
    }()
    
    // MARK: - Accessors
    class func shared() -> CoreDataController {
        return sharedCoreDataController
    }
    
    // MARK: - Inits
    override init(){
        super.init()
        
        self.startCalendarDB()
    }
    
    //- Mark: Main DB Controller Related Methods
    
    func startCalendarDB(){
        //check if managed context was declared correctly
        if(self.managedContext == nil){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.managedContext = appDelegate.persistentContainer.viewContext
        }
    }
    
    func saveCalendarDB() -> Bool{
        do {
            try self.getContextDB()!.save()
            return true
        } catch {
            print("Failure to save context: \(error)")
            return false
        }
    }
    
    func getContextDB() -> NSManagedObjectContext? {
        if(self.managedContext == nil){
            self.startCalendarDB()
        }
        
        return self.managedContext
    }
    
    //-Mark: Fetch Info from Calendar Core DB Methods
    
    func fetchRoomsFromDB() -> Array<Room>? {
        //fetch rooms
        let requestRooms = NSFetchRequest<NSFetchRequestResult>(entityName: "Room")
        requestRooms.returnsObjectsAsFaults = false
        do {
            let roomsFetched = try self.getContextDB()!.fetch(requestRooms) as? Array<Room>
            if(roomsFetched?.count ?? 0 > 0){
                //replace *displaying info for Rooms
                return roomsFetched
            }else{
                return nil
            }
        } catch {
            print("Failed to get part of general info from Core Data DB - Rooms query failed to fetch")
            return nil
        }
    }
    
    func fetchCaregiversFromDB() -> Array<Caregiver>? {
        //fetch caregivers
        let requestCaregivers = NSFetchRequest<NSFetchRequestResult>(entityName: "Caregiver")
        requestCaregivers.returnsObjectsAsFaults = false
        do {
            let caregiversFetched = try self.getContextDB()!.fetch(requestCaregivers) as? Array<Caregiver>
            if(caregiversFetched?.count ?? 0 > 0){
                return caregiversFetched
            }else{
                return nil
            }
        } catch {
            print("Failed to get part of general info from Core Data DB - Caregivers query failed to fetch")
            return nil
        }
    }
    
    func fetchAssignmentsFromDB(fromDatesToFetch datesToFetch:Array<Date>) -> Array<Assignment>{
        var assignmentsFetched: Array<Assignment> = []
        //fetch all dates different then dates displaying and passed filters of removal
        for dateToFech in datesToFetch {
            let fromDateUnformatted: Date = dateToFech.startOfDay
            let toDateUnformatted: Date = dateToFech.endOfDay
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Assignment")
            request.predicate = NSPredicate(format: "(time >= %@) AND (time <= %@)", fromDateUnformatted as NSDate, toDateUnformatted as NSDate)
            request.returnsObjectsAsFaults = false
            
            do {
                let result = try self.getContextDB()?.fetch(request)
                let newAssignmentsFetched = result as? [Assignment]
                
                //concat new assignments fetched into assignmentsDisplaying
                if(newAssignmentsFetched?.count ?? 0 > 0){
                    assignmentsFetched += newAssignmentsFetched!
                }
            } catch {
                print("Failed to fetch \"Assignment\" entities within dates ranges")
                return assignmentsFetched
            }
        }
        return assignmentsFetched
    }
    
    // - Mark: Delete Operation from Calendar Core DB
    
    func removeAssignmentFromDB(assignment assignmentToRemove: Assignment) -> Bool{
        self.getContextDB()?.delete(assignmentToRemove)
        return self.saveCalendarDB()
    }
    
    func clearAllAssignmentsFromDB() -> Bool{
        //remove all Assignments in Core Data DB
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Assignment")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            _ = try self.getContextDB()!.execute(request)
            _ = self.saveCalendarDB()
            return true
        } catch {
            print("Failed to clear all Assignments' entities from Core Data DB")
            return false
        }
    }
    
    func clearAllRoomsFromDB() -> Bool{
        //remove all Assignments in Core Data DB
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Room")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            _ = try self.getContextDB()!.execute(request)
            _ = self.saveCalendarDB()
            return true
        } catch {
            print("Failed to clear all Room's entities from Core Data DB")
            return false
        }
    }
    
    func clearAllCaregiversFromDB() -> Bool{
        //remove all Assignments in Core Data DB
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Caregiver")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            _ = try self.getContextDB()!.execute(request)
            _ = self.saveCalendarDB()
            return true
        } catch {
            print("Failed to clear all Caregiver's entities from Core Data DB")
            return false
        }
    }
    
    // - Mark: Save into Calendar's Core Data DB Methods (Only to facilitate view into other classes
    
    func saveAssignmentIntoCoreDataDB() -> Bool{
        return self.saveCalendarDB()
    }
    
    func saveCaregiversIntoCoreDataDB() -> Bool{
        return self.saveCalendarDB()
    }
    
    func saveRoomsIntoCoreDataDB() -> Bool{
        return self.saveCalendarDB()
    }
}
