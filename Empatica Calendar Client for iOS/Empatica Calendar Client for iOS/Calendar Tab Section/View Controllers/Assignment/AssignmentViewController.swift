//
//  AssignmentViewController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 19/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import CalendarKit

enum AssignmentVCMode : Int {
    case undefined = 0
    case newAssignment = 1
    case editEvent = 2
}

class AssignmentViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet var navigationTitleItem: UINavigationItem!
    @IBOutlet var deleteBtn: UIButton!
    
    @IBOutlet var dateTextField: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var caregiverTextField: UITextField!
    @IBOutlet var caregiverPicker: UIPickerView!
    
    @IBOutlet var roomTextField: UITextField!
    @IBOutlet var roomPicker: UIPickerView!
    
    var datePicked: Date?
    var eventDetails: EventView?
    var currentViewControllerMode: AssignmentVCMode?
    var selectedAssignment: Assignment?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //assign caregiver picker configs
        caregiverPicker.accessibilityLabel = "caregiver"
        caregiverPicker.delegate = self
        caregiverPicker.dataSource = self
        caregiverTextField.text = "1"
        
        //assign room picker configs
        roomPicker.delegate = self
        roomPicker.dataSource = self
        roomPicker.accessibilityLabel = "room"
        roomTextField.text = "1"

        //check what type of View Controller Mode this is
        if(datePicked != nil){
            //is it a new event or a event details' ViewController main purpose
            currentViewControllerMode = AssignmentVCMode.newAssignment
            
            //change to correct date
            dateTextField.text = datePicked!.format(with: "dd/MM/yyyy HH:mm")
            datePicker.date = datePicked!
            
            datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
            
            //Change Title of NavigationBar Label
            navigationTitleItem.title = "New Event"
            
            //Hide Delete Button
            deleteBtn.isEnabled = false
            deleteBtn.isHidden = true
            
            caregiverPicker.selectRow(50 - 1, inComponent: 0, animated: true)
            roomPicker.selectRow(5 - 1, inComponent: 0, animated: true)
        }else if (eventDetails != nil){
            //it is a event details view controller
            currentViewControllerMode = AssignmentVCMode.editEvent
            
            //Change Title of NavigationBar Label
            navigationTitleItem.title = "Event Details"
            
            //Show Delete Button
            deleteBtn.isHidden = false
            deleteBtn.isEnabled = true
            
            //Fill Date Picked with correct info from Event Details' var
            self.datePicked = self.eventDetails?.descriptor?.startDate
            
            //Get correct data from cached info
            let eventObj: Event? = self.eventDetails?.descriptor as? Event
            self.selectedAssignment = eventObj?.eventAssignmentEntity
            
            //put event date's picker (and TextField) into correct data stored
            dateTextField.text = datePicked!.format(with: "dd/MM/yyyy HH:mm")
            datePicker.date = datePicked!
            
            //put caregiver's picker (and TextField) into correct data stored
            caregiverPicker.selectRow(Int((self.selectedAssignment?.caregiverAssigned?.number ?? 50) - 1), inComponent: 0, animated: true)
            caregiverTextField.text = Int(self.selectedAssignment?.caregiverAssigned?.number ?? 50).description
            
            //put room's picker (and TextField) into correct data stored
            roomPicker.selectRow(Int((self.selectedAssignment?.roomAssigned?.roomNo ?? 5) - 1), inComponent: 0, animated: true)
            roomTextField.text = Int(self.selectedAssignment?.roomAssigned?.roomNo ?? 5).description
        }
    }
    
    // - Mark: Verification of Assignment eneligebility
    func canAssignmentBeMade() -> AssignmentEligebility {
        let resultRoomStatus: AssignmentEligebility = self.isRoomVacant()
        let resultCaregiverStatus: AssignmentEligebility = self.isCaregiverAvaliable()
        return AssignmentEligebility(rawValue: resultRoomStatus.rawValue + resultCaregiverStatus.rawValue) ?? AssignmentEligebility.undefined
    }
    
    func isRoomVacant() -> AssignmentEligebility {
        //get all rooms at this exact datetime's slot
        let slotDate: Date = datePicker.date
        var allAssignments: Array<Assignment> = CalendarDataController.shared().getAssignmentsAtSlot(ofHourAtDate: slotDate) ?? []
        
        //remove this event, in case this is an editing of an already existing event
        if(currentViewControllerMode == .editEvent){
            let index: Int? = allAssignments.firstIndex(where: {$0.objectID == self.selectedAssignment?.objectID})
            if(index != nil){
                allAssignments.remove(at: index!)
            }
        }
        
        let roomPicked: Int = (roomPicker.selectedRow(inComponent: 0) + 1)
        
        //check if room is already in use for this chosen date and time
        for assignmentUnit in allAssignments {
            let roomNumberAssigned: Int = Int(assignmentUnit.roomAssigned!.roomNo)
            if(roomNumberAssigned == roomPicked){
                return .NOK_ROOM_ALREADYUSED
            }
        }
        return .OK
    }
    
    func isCaregiverAvaliable() -> AssignmentEligebility {
        //get all caregivers at this exact datetime's slot
        let slotDate: Date = datePicker.date
        var allAssignments: Array<Assignment> = CalendarDataController.shared().getAssignmentsAtSlot(ofHourAtDate: slotDate) ?? []
        
        //remove this event, in case this is an editing of an already existing event
        if(currentViewControllerMode == .editEvent){
            let index: Int? = allAssignments.firstIndex(where: {$0.objectID.isEqual(self.selectedAssignment?.objectID)})
            if(index != nil){
                allAssignments.remove(at: index!)
            }
        }
        
        let caregiverPicked: Int = (caregiverPicker.selectedRow(inComponent: 0) + 1)
        let caregiverObj: Caregiver? = CalendarDataController.shared().getCaregiver(withNumber: caregiverPicked)
        
        //first check if this Caregiver is already in use for this chosen date and time
        for assignmentUnit in allAssignments {
            let caregiverAssigned: Int = Int(assignmentUnit.caregiverAssigned!.number)
            if(caregiverAssigned == caregiverPicked){
                return .NOK_CAREGIVER_ALREADYASSIGNED
            }
        }
        
        //also check if caregiver is elegible for being used, solely by its criterias
        if(caregiverObj != nil){
            //Is the Date chosen a working hour's period?
            if(slotDate.isDateAtWorkingTime() == true){
                //if so, check Criteria #1: A caregiver can work up to 5 hours per week
                if(caregiverObj!.hasReachedMaximumWorkingTimeHours(AtWeekDate: slotDate) == true){
                    return .NOK_CAREGIVER_OVERTIMEPERWEEKMAX
                }
            }
            
            //Is the Date chosen a overtime hour's period?
            if(slotDate.isDateOvertimeWork() == true){
                //If so, check Criteria #2: A caregiver can have a maximum of 1 overtime hour per week.
                if(caregiverObj!.hasReachedMaximumOvertimeHours(AtWeekDate: slotDate) == true){
                    return .NOK_CAREGIVER_OVERTIMEPERWEEKMAX
                }
            }
        }
        
        return .OK
    }
    
    // - Mark: Actions for UI Elements
    
    @IBAction func cancelBtnTouched(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTouched(_ sender: UIButton) {
        //save info at Model
        let caregiverPicked: Int = (caregiverPicker.selectedRow(inComponent: 0) + 1)
        let roomPicked: Int = (roomPicker.selectedRow(inComponent: 0) + 1)
        let newDate: Date = self.datePicker.date
        
        //can assignment be made?
        let assignmentStatus: AssignmentEligebility = canAssignmentBeMade()
        if(assignmentStatus != .OK){
            //there is something wrong, tell the user and cancel operation
            let assignmentStatusAlert = UIAlertController(title: "Error", message: assignmentStatus.getMessage(), preferredStyle: UIAlertController.Style.alert)
            assignmentStatusAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default: break
                case .cancel: break
                case .destructive: break
                }}))
            self.present(assignmentStatusAlert, animated: true, completion: nil)
        }else{
            if(currentViewControllerMode == AssignmentVCMode.newAssignment){
                CalendarDataController.shared().fillTemporaryAssignmentAndSave(withRoomNumber: roomPicked, andCaregiverNumber: caregiverPicked, atDate: newDate, andwithAssignmentByAI: false, andWithColor: nil) //color is already filled
            }else if(currentViewControllerMode == AssignmentVCMode.editEvent){
                //replace old assingment with new one (in cache and CoreDB), in case there was any changes
                CalendarDataController.shared().replaceAssignment(fromOldAssignment: self.selectedAssignment, toNewOneWithDate: newDate, andNewCaregiverNumber: caregiverPicked, andNewRoomNymber: roomPicked, andNewColor: Int(self.selectedAssignment?.color ?? 1))
            }
            
            self.eventDetails = nil
            self.datePicked = nil
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteBtnTouched(_ sender: UIButton) {
        //remove this event (if there is one)
        if(self.selectedAssignment != nil){
            CalendarDataController.shared().removeAssignment(assignment: self.selectedAssignment!)
        }
        
        self.eventDetails = nil
        self.datePicked = nil
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIPickerViewDataSource Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if(pickerView.accessibilityLabel == "caregiver"){
            return 1
        }else if(pickerView.accessibilityLabel == "room"){
            return 1
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.accessibilityLabel == "caregiver"){
            return 100
        }else if(pickerView.accessibilityLabel == "room"){
            return 10
        }
        return 0
    }
    
    // MARK: - UIPickerViewDelegate Methods, all optionals
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if(pickerView.accessibilityLabel == "caregiver"){
            return (row+1).description
        }else if(pickerView.accessibilityLabel == "room"){
            return (row+1).description
        }
        return "N/A"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView.accessibilityLabel == "caregiver"){
            caregiverTextField.text = (row+1).description
        }else if(pickerView.accessibilityLabel == "room"){
            roomTextField.text = (row+1).description
        }
    }
    
    // MARK: - DatePicker Value Changed Event Method
    
    @objc func datePickerChanged(picker: UIDatePicker) {
        picker.date.minute(0)
        dateTextField.text = picker.date.format(with: "dd/MM/yyyy HH:mm")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}

enum AssignmentEligebility : Int {
    case undefined = -1
    case OK = 0
    case NOK_ROOM_ALREADYUSED = 1
    case NOK_CAREGIVER_ALREADYASSIGNED = 2
    case NOK_ROOMANDCAREGIVER = 3
    case NOK_CAREGIVER_WORKHOURPERWEEKMAX = 4
    case NOK_CAREGIVER_OVERTIMEPERWEEKMAX = 5
    
    func getMessage() -> String {
        switch self.rawValue {
        case -1:
            return "Undefined reason."
        case 0:
            return "Caregiver and Room are OK."
        case 1:
            return "Room already booked for this Date and Time Slot! Please, choose a diferent one."
        case 2:
            return "Caregiver already assigned for this Date and Time Slot! Please, choose a diferent one."
        case 3:
            return "Both Room and Caregivers are already being used in this Date and Time Slot. Please, choose a different one for both of them."
        case 4:
            return "Caregiver chosen does not match at least one criteria. It has reached a limit on its working hours per week."
        case 5:
            return "Caregiver chosen does not match at least one criteria. It has reached a limit on its overtime hours per week."
        default:
            return "Unknown reason."
        }
    }
}
