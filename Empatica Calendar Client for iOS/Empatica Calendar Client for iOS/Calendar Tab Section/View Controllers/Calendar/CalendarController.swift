//
//  FirstViewController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 17/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit
import CalendarKit
import DateToolsSwift

class CalendarController: DayViewController {
    
    var calendarData: CalendarDataController?
    var rootController: UIViewController?
    
    var currentDateOnView: Date?
    
    var shouldRefreshContent: Bool?
    
    var colors = [UIColor.blue, UIColor.brown, UIColor.yellow, UIColor.green, UIColor.red, UIColor.orange]
    
    convenience init() {
        self.init(nibName:nil, bundle:nil)
        
        //configure all calendar view vars, delegates and data sources objects
        configureCalendarView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func calendarSubviewWillAppear() {
        //wait 2 seconds to disapear temporary assignment item, in case there was one
        if(CalendarDataController.shared().temporaryAssignment != nil){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                CalendarDataController.shared().temporaryAssignment = nil
                self.reloadEventsForDate()
            })
        }else{
            self.reloadEventsForDate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func configureCalendarView(){
        currentDateOnView = Date()
        dayView.dataSource = self
        dayView.delegate = self
        
        dayView.autoScrollToFirstEvent = true
        reloadEventsForDate()
    }
    
    func AutoAssignButtonTouched() {
        if(currentDateOnView != nil){
            CaregiverAutoAssignController.shared().fillSlotsForDay(ofDate: currentDateOnView!)
        }
    }
    
    func reloadEventsForDate(){
        shouldRefreshContent = true
        reloadData()
    }
    
    // MARK: EventDataSource
    
    override func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events = [Event]()
        let durationOfAttendance: Int = 60 //in minutes (1 hour)
        
        //fetch real assignments
        CalendarDataController.shared().fetchAssignments(nearDate: date, shouldRefreshContent: shouldRefreshContent)
        self.shouldRefreshContent = false
        let assignments: Array<Assignment>? = CalendarDataController.shared().getAssignments(forDate: date)
        
        //look for one temporary assignment, in case it was created
        let tempAssignment: TemporaryAssignment? = CalendarDataController.shared().getTemporaryAssignment(ifMatchesDateCriteria: date)
        if(tempAssignment != nil){
            let event = Event()
            let datePeriod = TimePeriod(beginning: tempAssignment!.time! as Date, chunk: TimeChunk.dateComponents(minutes: durationOfAttendance))
            
            event.startDate = datePeriod.beginning!
            event.endDate = datePeriod.end!
            
            var info = ["Caregiver: N/A" + " - Room #"]
            
            let timezone = TimeZone.ReferenceType.default
            info.append(datePeriod.beginning!.format(with: "dd.MM.YYYY", timeZone: timezone))
            info.append("\(datePeriod.beginning!.format(with: "HH:mm", timeZone: timezone)) - \(datePeriod.end!.format(with: "HH:mm", timeZone: timezone))")
            event.text = info.reduce("", {$0 + $1 + "\n"})
            event.color = colors[tempAssignment!.color!]
            event.isAllDay = false
            
            events.append(event)
        }
        
        //now look for real actual assignments
        for assignment in assignments! {
            let event = Event()
            let datePeriod = TimePeriod(beginning: assignment.time! as Date, chunk: TimeChunk.dateComponents(minutes: durationOfAttendance))
            
            //Add assingment entity (for later use inside assignment's View Controller)
            event.eventAssignmentEntity = assignment
            
            //add date info
            event.startDate = datePeriod.beginning!
            event.endDate = datePeriod.end!
            
            //Caregiver name, abbreviated like: Name X.
            var caregiverName: String = "N/A"
            if(assignment.caregiverAssigned?.name != nil){
                caregiverName = (assignment.caregiverAssigned?.name)!
                if let index = (caregiverName.range(of: " ")?.lowerBound)
                {
                    let index2 = caregiverName.index(index, offsetBy: 2)
                    caregiverName = String(caregiverName.prefix(upTo: index2) + ".")
                }
            }
            
            //Room #
            var roomNumber: String = " N/A"
            if(assignment.roomAssigned?.description != nil){
                roomNumber = (assignment.roomAssigned?.roomNo.description)!
            }
            var info = [caregiverName, "#" + roomNumber]
            
            let timezone = TimeZone.ReferenceType.default
            info.append(datePeriod.beginning!.format(with: "dd.MM.YYYY", timeZone: timezone))
            info.append("\(datePeriod.beginning!.format(with: "HH:mm", timeZone: timezone)) - \(datePeriod.end!.format(with: "HH:mm", timeZone: timezone))")
            
            //set all assignment information into the Event object
            //1st line of event: Caregiver name
            let attributedString = NSMutableAttributedString(string: "").attrStr(text: info[0] + "\n", font: UIFont.boldSystemFont(ofSize: 6), textColor: UIColor.darkGray)
            //2nd line: Caregiver photo
            //get document's path
            let textAttachment = NSTextAttachment()
            if(assignment.caregiverAssigned!.image != nil){
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(assignment.caregiverAssigned!.image!)
                    //load image into UIImage
                    let image = UIImage(contentsOfFile: imageURL.path)
                    //attach it to the event's image
                    textAttachment.image = image!.roundedImage
                }
            }else{
                //no image assignet, so put sample image
                textAttachment.image = UIImage(named: "sample_caregiver")
            }
            
            //scale it accordingly (make it tiny small)
            textAttachment.image = UIImage(cgImage: textAttachment.image!.cgImage!, scale: 3.7, orientation: .up)
            //append treated image to the attributed tring
            let attrStringWithImage = NSAttributedString(attachment: textAttachment)
            attributedString.append(attrStringWithImage)
            //3rd line: Room number
            _ = attributedString.attrStr(text: "\n" + info[1], font: UIFont.boldSystemFont(ofSize: 7), textColor: UIColor.darkGray)
            event.attributedText = attributedString
            
            //set event's visual attributes
            event.color = colors[Int(assignment.color)]
            event.isAllDay = false
            
            events.append(event)
        }
 
        return events
    }
    
    // MARK: DayViewDelegate
    override func dayViewDidLongPressTimelineAtHour(_ hour: Int) {
        if(currentDateOnView != nil){
            //format date to send, in case it exists
            let dateToSend: Date = Date(year: currentDateOnView!.year, month: currentDateOnView!.month, day: currentDateOnView!.day, hour: hour, minute: 0, second: 0)
            
            //add UI of a new blank event
            CalendarDataController.shared().addTemporaryAssignment(forTime: dateToSend)
            reloadEventsForDate()
            
            //present new view controller with date picked by the user already converted
            rootController?.performSegue(withIdentifier: "newAssignment", sender: dateToSend)
        }
    }
    
    override func dayViewDidSelectEventView(_ eventView: EventView) {
        guard (eventView.descriptor as? Event) != nil else {
            return
        }
        //present details view controller with event info
        rootController?.performSegue(withIdentifier: "eventDetails", sender: eventView)
    }
    
    override func dayViewDidLongPressEventView(_ eventView: EventView) {
        guard (eventView.descriptor as? Event) != nil else {
            return
        }
        
//        //present details view controller with event info
//        rootController?.performSegue(withIdentifier: "eventDetails", sender: eventView)
        if(currentDateOnView != nil){
            //format date to send, in case it exists
            let dateToSend: Date = Date(year: currentDateOnView!.year, month: currentDateOnView!.month, day: currentDateOnView!.day, hour: eventView.descriptor!.startDate.hour, minute: 0, second: 0)
            
            //add UI of a new blank event
            CalendarDataController.shared().addTemporaryAssignment(forTime: dateToSend)
            reloadEventsForDate()
            
            //present new view controller with date picked by the user already converted
            rootController?.performSegue(withIdentifier: "newAssignment", sender: dateToSend)
        }
    }
    
    override func dayView(dayView: DayView, willMoveTo date: Date) {
        
    }
    
    override func dayView(dayView: DayView, didMoveTo date: Date) {
        currentDateOnView = date
    }
    
}

extension UIImage {
    var roundedImage: UIImage {
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: self.size)
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: self.size.height
            ).addClip()
        self.draw(in: rect)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
}

extension Event {
    struct Static {
        static var key = "key"
    }
    var eventAssignmentEntity:Assignment? {
        get {
            return objc_getAssociatedObject( self, &Static.key ) as? Assignment
        }
        set {
            objc_setAssociatedObject( self, &Static.key,  newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension NSMutableAttributedString {
    func attrStr(text: String, font: UIFont, textColor: UIColor) -> NSMutableAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: textColor
        ]
        let string = NSMutableAttributedString(string: text, attributes: attributes)
        self.append(string)
        return self
    }
}
