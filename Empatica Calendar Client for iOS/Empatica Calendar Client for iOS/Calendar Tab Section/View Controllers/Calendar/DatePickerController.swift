//
//  DatePickerController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 19/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit

protocol DatePickerControllerDelegate: AnyObject {
    func datePicker(controller: DatePickerController, didSelect date: Date?)
}

class DatePickerController: UIViewController {
    
    weak var delegate: DatePickerControllerDelegate?
    
    var date: Date {
        get {
            return datePicker.date
        }
        set(value) {
            datePicker.setDate(value, animated: false)
        }
    }
    
    lazy var datePicker: UIDatePicker = {
        let v = UIDatePicker()
        v.datePickerMode = .date
        return v
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(DatePickerController.doneButtonDidTap))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                                                           target: self,
                                                           action: #selector(DatePickerController.cancelButtonDidTap))
        
        navigationController?.topViewController?.view.addSubview(datePicker)
        datePicker.frame = CGRect(x: 0, y: 0, width: navigationController?.topViewController?.view.frame.width ?? datePicker.frame.width, height: navigationController?.topViewController?.view.frame.height ?? datePicker.frame.height)
    }
    
    @objc func doneButtonDidTap() {
        delegate?.datePicker(controller: self, didSelect: date)
    }
    
    @objc func cancelButtonDidTap() {
        delegate?.datePicker(controller: self, didSelect: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
