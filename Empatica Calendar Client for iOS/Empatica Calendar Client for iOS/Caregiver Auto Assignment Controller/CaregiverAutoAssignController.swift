//
//  CaregiverController.swift
//  Empatica Calendar Client for iOS
//
//  Created by Lucas on 22/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import UIKit

class CaregiverAutoAssignController: NSObject {
    
    //Rules
    let distanceCostPerRooms = 1 //diff(abs(room1, room2)) = 1
    
    //Auto-assign controller for caregivers in a specific day
    
    //Priority:
    //1) Caregiver with no overtime
    //2) Caregiver working on the same day, nearest room
    //3) Caregiver with less hours worked in the last 4 weeks
    //4) If no criteria match, Slot will be vacant
    
    var isWorking: Bool = false
    
    //- Mark: Singleton Design Pattern related methods
    
    // MARK: - Properties
    private static var sharedCaregiverAutoAssignController: CaregiverAutoAssignController = {
        let autoAssignController = CaregiverAutoAssignController()
        
        return autoAssignController
    }()
    
    // MARK: - Accessors
    class func shared() -> CaregiverAutoAssignController {
        return sharedCaregiverAutoAssignController
    }
    
    // MARK: - Inits
    override init(){
        super.init()
        
    }
    
    func fillSlotsForDay(ofDate datetime: Date){
        self.isWorking = true
        
        //create all possible slots' Dates in an array
        let allPossibleSlots: [Date] = self.createAllDailySlots(forDate: datetime) ?? []
        
        //sort all caregivers by total working hours per week (ascending)
        let allCaregivers: Array<Caregiver>? = CalendarDataController.shared().getAllCaregivers()
        var caregiversSorted: [Dictionary<String,Any>] = []
        for caregiverUnit in allCaregivers!{
            caregiversSorted.append([ "monthHoursWorked": caregiverUnit.getTotalHoursWorkingAtMonth(ofDate: datetime), "CaregiverObj": caregiverUnit ])
        }
        caregiversSorted.sort(by: { ($0["monthHoursWorked"] as! Int) < ($1["monthHoursWorked"] as! Int ) })
        
        //group and put caregivers with overtime hour at the end of the list
        let (withOvertimeArray, nonOvertimeArray) = caregiversSorted.stablePartition { ($0["CaregiverObj"] as! Caregiver).hasOverTimeHourAtWeek(ofDate: datetime) == true }
        caregiversSorted = []
        caregiversSorted.append(contentsOf: nonOvertimeArray)
        caregiversSorted.append(contentsOf: withOvertimeArray)
        
        //from room 1 to 10, iterate thru all vacant hours:
        let totalRooms: Int = CalendarDataController.shared().maxRoomsPerFloor * CalendarDataController.shared().maxFloors
        for dateSlot in allPossibleSlots{
            for roomIndex in 1...totalRooms{
                //check if this room is already assigned for this Date's Slot
                if(self.isRoomAlreadyAssignedForDateSlot(roomNumberToCheck: roomIndex, dateSlotToCompre: dateSlot) == true){
                    //it is assigned, so skipthis room iteration's unit
                    continue
                }
                
                //check if there is any caregiver assigned, until end of working hours
                var dayAssignments: Array<Assignment> = CalendarDataController.shared().getAssignments(forDate: dateSlot) ?? []
                
                //sort the ones closer by distance to the room in question, giving preference to the ones with minus distance
                dayAssignments.sort(by: { $0.getDistance(ToRoomNumber: roomIndex) < $1.getDistance(ToRoomNumber: roomIndex) })
                
                //remove caregivers that already exceeded criterias (max hours allowed to work, regular and overtime)
                dayAssignments.removeAll(where: { $0.caregiverAssigned?.hasReachedMaximumWorkingTimeHours(AtWeekDate:dateSlot) ?? false || $0.caregiverAssigned?.hasReachedMaximumOvertimeHours(AtWeekDate:dateSlot) ?? false })
                
                //pick the one with the least amount of distances, if exist
                var bestCaregiverToAssign: Caregiver?
                for shortestDistanceAssignment in dayAssignments {
                    if(shortestDistanceAssignment.caregiverAssigned != nil){
                        //check if this caregiver is already working at this DateSlot (criteria #2)
                        if(shortestDistanceAssignment.caregiverAssigned?.isCaregiverAlreadyAssignedForDateSlot(dateSlotToCompre: dateSlot) == false){
                            //it is not, so this is the best-fit caregiver to assign
                            bestCaregiverToAssign = shortestDistanceAssignment.caregiverAssigned
                            break
                        }
                    }
                }
                
                //if does not exist any best fit yet, try now to pick one from the caregiversSorted's list
                if(bestCaregiverToAssign == nil){
                    //remove caregivers that already exceeded criterias (max hours allowed to work, regular and overtime)
                    caregiversSorted.removeAll(where: { ($0["CaregiverObj"] as! Caregiver).hasReachedMaximumWorkingTimeHours(AtWeekDate:dateSlot) || ($0["CaregiverObj"] as! Caregiver).hasReachedMaximumOvertimeHours(AtWeekDate:dateSlot) })
                    
                    for caregiverSortedUnit in caregiversSorted {
                        if(caregiverSortedUnit["CaregiverObj"] != nil){
                            //check if this caregiver is already working at this DateSlot (criteria #2)
                            let candidateCaregiver: Caregiver? = caregiverSortedUnit["CaregiverObj"] as? Caregiver
                            if(candidateCaregiver?.isCaregiverAlreadyAssignedForDateSlot(dateSlotToCompre: dateSlot) == false){
                                bestCaregiverToAssign = caregiverSortedUnit["CaregiverObj"] as? Caregiver
                                break
                            }
                        }
                    }
                }
                
                //if picked one caregiver, continue:
                if(bestCaregiverToAssign != nil){
                    //now, create this new assignment into CalendarDataController Local Mem var
                    _ = CalendarDataController.shared().createNewAssignment(atDateTime: dateSlot, withCaregiver: Int(bestCaregiverToAssign!.number), andWithRoom: roomIndex)
                }
            }
        }
        
        //now that all were added to Local Mem's var, add all new assignment (in case there are any new ones) to coreDB
        _ = CoreDataController.shared().saveCalendarDB()
        
        self.isWorking = false
    }
    
    func createAllDailySlots(forDate datetime: Date) -> Array<Date>?{
        //create a list of all daily slots
        var datesToReturn: Array<Date> = []
        
        for i in 9...16 {
            var newDateSlot: Date = datetime.startOfDay
            newDateSlot.hour(i)
            datesToReturn.append(newDateSlot)
        }
        
        return datesToReturn
    }
    
    func isRoomAlreadyAssignedForDateSlot(roomNumberToCheck roomNumber: Int, dateSlotToCompre dateSlot: Date) -> Bool {
        let assignmentsAtSlot: Array<Assignment> = CalendarDataController.shared().getAssignmentsAtSlot(ofHourAtDate: dateSlot) ?? []
        for assignmentUnit in assignmentsAtSlot {
            let roomNumberAssigned: Int = Int(assignmentUnit.roomAssigned!.roomNo)
            if(roomNumberAssigned == roomNumber){
                //room in use found, so return that this room is already assigned
                return true
            }
        }
        return false
    }
    
}

extension Caregiver {
    
    func getTotalHoursWorkingAtMonth(ofDate datetime: Date) -> Int {
        return self.getAllAssignmentsWorkingAtMonth(ofDate: datetime).count
    }
    
    func getAllAssignmentsWorkingAtMonth(ofDate datetime: Date) -> Array<Assignment> {
        let beginningMonthDate: Date = datetime.startOfMonth()
        let endMonthDate: Date = datetime.endOfMonth()
        
        var assignmentsAtMonth: Array<Assignment> = []
        
        if(self.assignments != nil){
            for objUnit in self.assignments! {
                let assignment = objUnit as! Assignment
                let assignmentDate: Date = assignment.time! as Date
                if(assignmentDate >= beginningMonthDate && assignmentDate < endMonthDate){
                    assignmentsAtMonth.append(assignment)
                }
            }
        }
        
        return assignmentsAtMonth
    }
    
    func getAllAssignmentsWorkingAtWeek(ofDate datetime: Date) -> Array<Assignment> {
        //PS: firstWeekDay is Monday, last week day
        
        let beginningWeekDate: Date = datetime.isTheWeekDay(weekDayToCheck: .monday) ? datetime.startOfDay : datetime.previous(.monday).startOfDay
        let endWeekDate: Date = datetime.isTheWeekDay(weekDayToCheck: .sunday) ? datetime.endOfDay : datetime.next(.sunday).endOfDay
        
        var assignmentsAtWeek: Array<Assignment> = []
        
        if(self.assignments != nil){
            for objUnit in self.assignments! {
                let assignment = objUnit as! Assignment
                let assignmentDate: Date = assignment.time! as Date
                if(assignmentDate >= beginningWeekDate && assignmentDate < endWeekDate){
                    assignmentsAtWeek.append(assignment)
                }
            }
        }
        
        return assignmentsAtWeek
    }
    
    func getTotalHoursWorkedAtWeek(ofDate datetime: Date) -> Int{ //firstWeekDay is Monday, last week day is Sunday
        return self.getAllAssignmentsWorkingAtWeek(ofDate: datetime).count
    }
    
    func getTotalWorkingTimeHoursWorkedAtWeek(ofDate datetime: Date) -> Int{
        let totalWorkingAssignments: Array<Assignment> = self.getAllAssignmentsWorkingAtWeek(ofDate: datetime)
        var workingTimeCount: Int = 0
        for workingAssignment in totalWorkingAssignments {
            let workingDate: Date = workingAssignment.time! as Date
            if(workingDate.isDateAtWorkingTime()){
                workingTimeCount += 1
            }
        }
        return workingTimeCount
    }
    
    func getTotalHourWorkedOvertimeAtWeek(ofDate datetime: Date) -> Int {
        return self.getTotalHoursWorkedAtWeek(ofDate: datetime) - self.getTotalWorkingTimeHoursWorkedAtWeek(ofDate: datetime)
    }
    
    func hasOverTimeHourAtWeek(ofDate datetime: Date) -> Bool{
        return (getTotalHourWorkedOvertimeAtWeek(ofDate: datetime)) > 0 ? true : false
    }
    
    func hasReachedMaximumWorkingTimeHours(AtWeekDate datetime:Date) -> Bool {
        return self.getTotalWorkingTimeHoursWorkedAtWeek(ofDate: datetime) >= 5 ? true: false
    }
    
    func hasReachedMaximumOvertimeHours(AtWeekDate datetime:Date) -> Bool {
        return self.getTotalHourWorkedOvertimeAtWeek(ofDate: datetime) >= 1 ? true : false
    }
    
    func canWorkAt(possibleNewAssignmentDate datetime: Date) -> Bool{
        if((self.hasReachedMaximumWorkingTimeHours(AtWeekDate: datetime) == false) && (self.hasReachedMaximumOvertimeHours(AtWeekDate: datetime) == false)){
            return true
        }
        
        return false
    }
    
    func isCaregiverAlreadyAssignedForDateSlot(dateSlotToCompre dateSlot: Date) -> Bool{
        if(self.assignments != nil){
            for assignmentUnit in self.assignments! {
                if((assignmentUnit as! Assignment).time != nil){
                    if(((assignmentUnit as! Assignment).time! as Date).equals(dateSlot)){
                        return true
                    }
                }
            }
        }
        return false
    }
}

extension Date {
    
    func isTheWeekDay(weekDayToCheck: Weekday) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ccc"
        let dateOfWeekString = dateFormatter.string(from: self)
        
        var weekDay: String = ""
        switch weekDayToCheck {
        case .monday:
            weekDay = "Mon"
        case .tuesday:
            weekDay = "Tue"
        case.wednesday:
            weekDay = "Wed"
        case.thursday:
            weekDay = "Thu"
        case.friday:
            weekDay = "Fri"
        case.saturday:
            weekDay = "Sat"
        case.sunday:
            weekDay = "Sun"
        }
        
        if(weekDay == dateOfWeekString) {
            return true
        }
        return false
    }
    
    static func today() -> Date {
        return Date()
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func isDateAtWorkingTime() -> Bool {
        if(self.hour >= 9 && self.hour <= 16){
            return true
        }
        return false
    }
    
    func isDateOvertimeWork() -> Bool {
        //inverse of is working time hours = is overtime hours
        return !self.isDateAtWorkingTime()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    func get(_ direction: SearchDirection,
             _ weekDay: Weekday,
             considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        
        let searchWeekdayIndex = weekdaysName.index(of: dayName)! + 1
        
        let calendar = Calendar(identifier: .gregorian)
        
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        
        var nextDateComponent = DateComponents()
        nextDateComponent.weekday = searchWeekdayIndex
        
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
    
}

// MARK: Helper methods
extension Date {
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    enum SearchDirection {
        case Next
        case Previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .Next:
                return .forward
            case .Previous:
                return .backward
            }
        }
    }
}

extension Array {
    
    mutating func sendElementToTheEnd(fromIndex indexToSend: Int){
        let element = self.remove(at: indexToSend)
        self.append(element)
    }
    
    func stablePartition(by condition: (Element) -> Bool) -> ([Element], [Element]) {
        var matching = [Element]()
        var nonMatching = [Element]()
        for element in self {
            if condition(element) {
                matching.append(element)
            } else {
                nonMatching.append(element)
            }
        }
        return (matching, nonMatching)
    }
}

extension Assignment {
    
    func getDistance(ToRoomNumber roomNumber:Int) -> Int{
        if(self.roomAssigned != nil){
            return abs(Int(self.roomAssigned!.roomNo) - roomNumber)
        }
        return 0
    }
    
}
