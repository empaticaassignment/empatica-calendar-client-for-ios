//
//  Assignment+CoreDataProperties.swift
//  
//
//  Created by Lucas on 21/10/2018.
//
//

import Foundation
import CoreData


extension Assignment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Assignment> {
        return NSFetchRequest<Assignment>(entityName: "Assignment")
    }

    @NSManaged public var aiGen: Bool
    @NSManaged public var color: Int32
    @NSManaged public var time: NSDate?
    @NSManaged public var id: UUID?
    @NSManaged public var caregiverAssigned: Caregiver?
    @NSManaged public var roomAssigned: Room?

}
