//
//  Caregiver+CoreDataProperties.swift
//  
//
//  Created by Lucas on 21/10/2018.
//
//

import Foundation
import CoreData


extension Caregiver {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Caregiver> {
        return NSFetchRequest<Caregiver>(entityName: "Caregiver")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var image: String?
    @NSManaged public var name: String?
    @NSManaged public var number: Int16
    @NSManaged public var assignments: NSSet?

}

// MARK: Generated accessors for assignments
extension Caregiver {

    @objc(addAssignmentsObject:)
    @NSManaged public func addToAssignments(_ value: Assignment)

    @objc(removeAssignmentsObject:)
    @NSManaged public func removeFromAssignments(_ value: Assignment)

    @objc(addAssignments:)
    @NSManaged public func addToAssignments(_ values: NSSet)

    @objc(removeAssignments:)
    @NSManaged public func removeFromAssignments(_ values: NSSet)

}
