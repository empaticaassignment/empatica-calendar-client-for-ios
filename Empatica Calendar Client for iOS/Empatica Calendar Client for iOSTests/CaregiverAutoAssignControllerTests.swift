//
//  CaregiverAutoAssignControllerTests.swift
//  Empatica Calendar Client for iOSTests
//
//  Created by Lucas on 24/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import XCTest

class CaregiverAutoAssignControllerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Mark - Check Singleton Design Pattern

    func testIfCaregiverAutoAssignObjectIsASingleton() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let caregiverASObjSingleton : CaregiverAutoAssignController? = CaregiverAutoAssignController.shared()
        XCTAssertTrue(caregiverASObjSingleton?.isKind(of: CaregiverAutoAssignController.self) ?? false && caregiverASObjSingleton != nil, "Should be a Singleton")
    }
    
    // Mark - Check Important methods
    
    func testDateSlotsCreationForDay() {
        let dateToTest : Date = Date() //today
        let dateSlots: Array<Date>? = CaregiverAutoAssignController.shared().createAllDailySlots(forDate: dateToTest)
        XCTAssertTrue(dateSlots != nil, "is not returning any Date Slots at all")
        XCTAssertTrue(dateSlots?.count == 8, "is not returning the correct amount of Date Slots")
        for i in 9...16{
            let indexReturned: Int? = dateSlots?.firstIndex(where: {$0.hour == i})
            XCTAssertTrue(indexReturned != nil, "is not filling the correct dates at Slots")
        }
    }
    
    // Mark - Check important Date extensions creaed
    
    func testFirstDayOfMonthDate(){
        let dateToTest : Date = Date(year: 2018, month: 10, day: 23, hour: 01, minute: 30, second: 15)
        
        let startOfMonthDate: Date = dateToTest.startOfMonth()
        let dateToVerify : Date = Date(year: 2018, month: 10, day: 01, hour: 00, minute: 00, second: 00)
        
        XCTAssertTrue(startOfMonthDate.equals(dateToVerify), "is not correctly giving the first day of the month")
    }
    
    func testLastDayOfMonthDate(){
        let dateToTest : Date = Date(year: 2018, month: 10, day: 23, hour: 01, minute: 30, second: 15)
        
        let endOfMonthDate: Date = dateToTest.endOfMonth()
        let dateToVerify : Date = Date(year: 2018, month: 10, day: 31, hour: 00, minute: 00, second: 00)
        
        XCTAssertTrue(endOfMonthDate.equals(dateToVerify), "is not correctly giving the last day of the month")
    }
    
    func testDateIsOvertimeHourForCaregiver(){
        let dateToTest : Date = Date(year: 2018, month: 10, day: 23, hour: 01, minute: 00, second: 00)
        XCTAssertTrue(dateToTest.isDateOvertimeWork(), "is not correctly detecting when hour is overtime")
    }
    
    func testDateIsRegularWorkHourForCaregiver(){
        let dateToTest : Date = Date(year: 2018, month: 10, day: 23, hour: 12, minute: 00, second: 00)
        XCTAssertTrue(dateToTest.isDateAtWorkingTime(), "is not correctly detecting when hour is a regular hour")
    }
    
    func testIfDateIsBeginningOfWeek(){
        //Beginning of the week is considered to be Monday
        let nonWeekBeginning : Date = Date(year: 2018, month: 10, day: 23, hour: 00, minute: 00, second: 00)
        let weekBeginningDay : Date = Date(year: 2018, month: 10, day: 22, hour: 00, minute: 00, second: 00)
        
        XCTAssertFalse(nonWeekBeginning.isTheWeekDay(weekDayToCheck: .monday), "Non-beginning of week's date not showing correctly as a \"Non-Monday\"")
        XCTAssertTrue(weekBeginningDay.isTheWeekDay(weekDayToCheck: .monday), "Beginning of week's date not showing correctly as a Monday")
    }
    
    func testIfDateIsEndOfWeek(){
        //End of Week is considered to be Sunday
        let nonWeekEnding : Date = Date(year: 2018, month: 10, day: 24, hour: 00, minute: 00, second: 00)
        let weekEndingDay : Date = Date(year: 2018, month: 10, day: 21, hour: 00, minute: 00, second: 00)
        
        XCTAssertFalse(nonWeekEnding.isTheWeekDay(weekDayToCheck: .sunday), "Non-ending of week's date not showing correctly as a \"Non-Sunday\"")
        XCTAssertTrue(weekEndingDay.isTheWeekDay(weekDayToCheck: .sunday), "End of week's date not showing correctly as a Sunday")
    }
    
}
