//
//  Empatica_Calendar_Client_for_iOSUITests.swift
//  Empatica Calendar Client for iOSUITests
//
//  Created by Lucas on 17/10/2018.
//  Copyright © 2018 Lucas. All rights reserved.
//

import XCTest

class Empatica_Calendar_Client_for_iOSUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false

        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCheckingTwoTabs() {
        app.launch()
        
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["About"].tap()
        XCTAssert(app.navigationBars["Calendar"].exists, "is not entering correctly into About tab from Main Screen")
        
        let tabBarsQuery2 = app.tabBars
        tabBarsQuery2.buttons["Calendar"].tap()
        XCTAssert(app.navigationBars["Calendar"].exists, "is not going back correctly into Calendar tab from About Screen")
        
    }
}
