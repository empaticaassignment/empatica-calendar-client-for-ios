# Empatica Calendar Client for iOS
## This is the client side for Empatica's Calendar Assignment.
#### Written for iOS via Swift by Lucas Quintiliano Prates.

---
To use it, you will need to download this repo, using the **release** tag to get the latest stable version.

You also need to download and install the CocoaPods decency manager at https://cocoapods.org.

Instructions on how to download and install it are located at the webpage.
I recommend you install it using the **gems** method.


After done installing, open the Terminal App again and enter on the main subdirectory of the entire XCode project. There, type and execute:

```> Pod install ```

This will download all dependent external frameworks to their latest versions. After all all installation are done, open the Xcode Project, change the identifier and owner of the Project at Project Settings. Build and execute on your preferred iOS device.

### Instructions on how to use the Application:

While executing the App, if you want to add a new assignment to that particular day, just touch the hour, long pressing, and a window will appear with all the information you can edit and insert.
After tapping Done, you will return to the screen and be able to manage the calendar like before, now with a new assignment. <br>

####**Attention! Before executing the code, don't forget to read this other simple instructions below:**
* ** If you want to add a new assignment on the same line that one is already assigned, just touch long pressing the event and a new assignment screen will appear for that same time.**
* ** Auto assign feature does take longer than expected to fill. So hold tight that depending on the device's processor, it can take almost 20 seconds to fill all the 80 assignments for that day. You can still go do other things while waiting, though.**
<br><br>

####News: The bugs found yesterday was fixed:
* There was one bug that I decided to leave in release version 1.0.1. Today, I fixed the bug. It took less than 30 minutes to spot it, think of a solution, implement it, and test it. I even created a XCTest assert for this bug to never give an error at production code never again.
<br><br>

####Informations about the code:
* The entire code was written with the quickness to deliver in mind. So only two files have the tests of UI and Programatically driven logic done. If it was an actual production code, all files and functions would have been tested and the development mindset would have been TDD (Test Driven Development).
<br><br>

#####Any further questions, don't hesitate to contact me at: 
######empaticacalendarproject@lukequinn.com

---

Lucas Quintiliano Prates - 2018 All Rights Reserved
